package a3consumer;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

import com.consumer.service.MailService;
import com.consumer.service.Serializer;
import com.producer.data.DVD;
import com.rabbitmq.client.*;

public class Consumer {
	
	private static final String TASK_QUEUE_NAME = "task_queue";
    private static int i = 0;
    public static void main( String[] argv )
            throws IOException,
            InterruptedException, TimeoutException{
    	
    	String customers[] = new String [2];
    	customers[0] = "kickisiku_ro@yahoo.com";
    	

		MailService mailService = new MailService("email","password");

        ExecutorService threader = Executors.newFixedThreadPool(20);
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection(threader);
        final Channel channel = connection.createChannel();

        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        channel.basicQos(20);

        final QueueingConsumer consumer = new QueueingConsumer(channel);
        channel.basicConsume(TASK_QUEUE_NAME, false, consumer);

        try {

            while (true) {

                        try {QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                            DVD dvd = (DVD) DVD.convertFromBytes(delivery.getBody());
                        
                            
            				System.out.println("Sending mail "+dvd.toString());
            				
            				for(int i = 0 ; i < customers.length; i++){
            				mailService.sendMail(customers[i],"New DVD available:", dvd.toString());
            				Serializer.serializeDVD(dvd);
            				}
            				for(int i = 0 ; i < customers.length; i++){
                            System.out.println(" [" + (customers[i]) +"] received " + dvd.toString());
            				}
                            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                        }catch (Exception e){
                        }
                    }
        } catch (Exception e){
            e.printStackTrace();
        }
        channel.close();
        connection.close();
    }
	
}
