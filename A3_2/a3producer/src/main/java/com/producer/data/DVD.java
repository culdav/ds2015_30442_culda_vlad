package com.producer.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class DVD implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String title;
	private int year;
	private double price;
	
	public DVD(String title, int year, double price){
		this.title = title;
		this.year = year; 
		this.price = price;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String toString(){
		return ("Title:" + this.title + "\nYear: " + this.year + "\nPrice: " + this.price); 
		
	}
	
	public byte[] convertToBytes(Object object) throws IOException {
		     ByteArrayOutputStream bos = new ByteArrayOutputStream();
		         ObjectOutput out = new ObjectOutputStream(bos);
		        out.writeObject(object);
		        return bos.toByteArray();
		    
		}
	    
	public static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
	        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
	             ObjectInput in = new ObjectInputStream(bis);
	            return in.readObject();
	        
	    }
	
	
}

