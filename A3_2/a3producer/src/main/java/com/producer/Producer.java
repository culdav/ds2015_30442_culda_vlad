package com.producer;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import com.rabbitmq.client.Connection;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.*;
import java.util.concurrent.TimeoutException;

import com.producer.data.DVD;
import com.rabbitmq.client.Channel;

public class Producer {
		
	
	private static final String TASK_QUEUE_NAME = "task_queue";

    public static void main( String[] argv) throws IOException, ClassNotFoundException, TimeoutException{

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        
        channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

        Boolean finish = false;
        
        DVD dvd = new DVD(argv[0], Integer.parseInt(argv[1]),Double.parseDouble(argv[2]));
        do {
            
            byte [] byteMessage = dvd.convertToBytes(dvd);
       
            channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, byteMessage);
            System.out.println("Message Sent" + DVD.convertFromBytes(byteMessage).toString());
            finish = true;
        } while (!finish);

        channel.close();
        connection.close();
    }

//    private static DVD getDVD(){
//        DVD dvd = new DVD("ROCK DVD", 1993, 200);
//        return dvd;
//    }
    
    
    
}
