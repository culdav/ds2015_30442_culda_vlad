package com.businesslogic;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.producer.Producer;
import com.producer.data.DVD;

public class A3Logic {
	
	private DVD dvd;
	
	public A3Logic(DVD dvd){
		this.dvd = dvd;
	}
	
	public void startLogic() throws ClassNotFoundException, IOException, TimeoutException{
		String args[] = new String[3]; 
		args[0] = dvd.getTitle();
		args[1] = dvd.getYear() + "" ;
		args[2] = dvd.getPrice() + "";
		Producer.main(args);
	}
	
}
