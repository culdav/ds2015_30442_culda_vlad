package com.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.businesslogic.A3Logic;
import com.producer.data.DVD;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import java.awt.event.ActionEvent;

public class A3Admin extends JFrame {

	private JPanel contentPane;
	private JTextField titleText;
	private JTextField yearText;
	private JTextField priceText;
	private A3Logic logicUtils;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A3Admin frame = new A3Admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public A3Admin() {
		setTitle("A3");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitle = new JLabel("Title");
		lblTitle.setBounds(10, 26, 35, 14);
		contentPane.add(lblTitle);
		
		JLabel lblYear = new JLabel("Year");
		lblYear.setBounds(10, 57, 46, 14);
		contentPane.add(lblYear);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(10, 91, 46, 14);
		contentPane.add(lblPrice);
		
		titleText = new JTextField();
		titleText.setBounds(55, 26, 86, 20);
		contentPane.add(titleText);
		titleText.setColumns(10);
		
		yearText = new JTextField();
		yearText.setBounds(55, 54, 86, 20);
		contentPane.add(yearText);
		yearText.setColumns(10);
		
		priceText = new JTextField();
		priceText.setBounds(55, 88, 86, 20);
		contentPane.add(priceText);
		priceText.setColumns(10);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DVD dvd = new DVD(titleText.getText(), Integer.parseInt(yearText.getText()), Integer.parseInt(priceText.getText()));
				logicUtils = new A3Logic(dvd);
				try {
					logicUtils.startLogic();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (TimeoutException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAdd.setBounds(166, 53, 89, 23);
		contentPane.add(btnAdd);
		
		JLabel messageLabel = new JLabel("");
		messageLabel.setBounds(283, 57, 141, 14);
		contentPane.add(messageLabel);
	}
}
