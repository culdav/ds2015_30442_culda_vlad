package servlets;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import BusinessLogic.FlightBO;
import Entities.Flight;
import Presentation.AdminView;
import Presentation.View;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet(name= "UserServlet" )
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private View view = null;
    private String username;
    private String password;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 PrintWriter out = response.getWriter();
		 
		 		 
		if(request.getRequestURI().equals("/tutorial_1.2/test"))
	        {
			
			  username = request.getParameter("firstName");
	          password  = request.getParameter("lastName");
	          View view = new View(username,password);

	            if(view != null)
	            {
	            	 out.println(view.Init());
	            }
	            else
	            {
	                out.println("Check username and password");
	            }
	        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 PrintWriter out = response.getWriter();
		
		 if (request.getRequestURI().equals("/tutorial_1.2/addNewFlight")){
			 String flightNumber 	= request.getParameter("flightNr");
	         String airplaneType 	= request.getParameter("airplaneType");
	         String departureDate 	= request.getParameter("departDate");
	         String departureHour 	= request.getParameter("departTime");
	         String arrivalCity 	= request.getParameter("arrivalCity");
	         String arrivalDate 	= request.getParameter("arrivalDate");
	         String arrivalHour 	= request.getParameter("arrivalHour");
	         String departureCity 	= request.getParameter("departCity");
	         
	         Flight flight = new Flight();
	         Random r = new Random();
	         flight.setFlightNr(flightNumber);
	         flight.setAirplaneType(airplaneType);
	         flight.setArrivalCity(arrivalCity);
	         flight.setArrivalDate(arrivalDate);
	         flight.setArrivalHour(arrivalHour);
	         flight.setDepartCity(departureCity);
	         flight.setDepartDate(departureDate);
	         flight.setDepartTime(departureHour);
	         
	         
	         AdminView adminView = new AdminView();
	         adminView.AddFlight(flight);
	         out.println("Flight added!");
	         
		 }
		 
		 if (request.getRequestURI().equals("/tutorial_1.2/removeSelectedFlights"))
	        {
			 
	            if (request.getParameter("submit1") != null)
	            {
	                String[] flight;
	                ArrayList<String> flightsNumberToDelete = new ArrayList<String>();
	                String[] selectedFlights                = request.getParameterValues("flights");
	                
	                if (selectedFlights != null)
	                {
	                    for (String flights : selectedFlights)
	                    {
	                        flight = flights.split(" ");
	                        
	                        flightsNumberToDelete.add(flight[2].replaceAll("[\\D]", ""));
	                    }
	                    View adminView = new View(username,password);
	                    AdminView aV = new AdminView();
	                    aV.deleteFlights(flightsNumberToDelete);
	                    //out.println(adminView.Init());
	                }
	                out.println("Flight deleted!");
	            }
	       }
	    

		
	}

}

