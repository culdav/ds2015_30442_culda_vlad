package Entities;

import javax.persistence.Entity;

@Entity
public class City {

	
	private int cityId;
	private String cityname;
	private String lat;
	private String longitude;
	
	public City(){
		
	}
	
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public String getCityname() {
		return cityname;
	}
	public void setCityname(String cityname) {
		this.cityname = cityname;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	@Override
	public String toString() {
		
		return "City [cityId=" + cityId + ", cityname=" + cityname + ", lat=" + lat + ", longitude=" + longitude +"]";
	}
	
	
	
	
}
