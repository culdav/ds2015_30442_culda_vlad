package Entities;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Flight {
	
	private int id;
	private String flightNr;
	private String airplaneType;
	
	@ManyToOne
    @JoinColumn(name="city_name")
	private String departCity;
	private String departDate;
	private String departTime;
	
	@ManyToOne
    @JoinColumn(name="city_name")
	private String arrivalCity;
	private String arrivalDate;
	private String arrivalHour;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFlightNr() {
		return flightNr;
	}
	public void setFlightNr(String flightNr) {
		this.flightNr = flightNr;
	}
	public String getAirplaneType() {
		return airplaneType;
	}
	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}
	public String getDepartDate() {
		return departDate;
	}
	public void setDepartDate(String departDate) {
		this.departDate = departDate;
	}
	public String getDepartTime() {
		return departTime;
	}
	public void setDepartTime(String departTime) {
		this.departTime = departTime;
	}
	public String getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public String getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public String getArrivalHour() {
		return arrivalHour;
	}
	public void setArrivalHour(String arrivalHour) {
		this.arrivalHour = arrivalHour;
	}
	public String getDepartCity() {
		return departCity;
	}
	public void setDepartCity(String departCity) {
		this.departCity = departCity;
	}
	
	@Override
	public String toString() {
		return "Flight [id=" + id + ", flightNr=" + flightNr + ", airplaneType=" + airplaneType + ", departCity=" + departCity +", departDate="
				+ departDate + ", departTime=" + departTime + ", arrivalCity=" + arrivalCity + ", arrivalDate="
				+ arrivalDate + ", arrivalHour=" + arrivalHour + "]";
	}
	
	
	
	
}
