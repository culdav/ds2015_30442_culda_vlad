package Entities;

public class User {
	
	private int    userid;
	private String username;
	private String password;
	private String type;
	
	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(String username, String password, String type) {
		
		this.username = username;
		this.password = password;
		this.type = type;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
	
}
