package DataAccess;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import Entities.Flight;

public class FlightDAO {
	
	private SessionFactory factory;
	
	public FlightDAO(SessionFactory factory){
		this.factory = factory;
	}
	
	
	public List<Flight> getAll(){
		
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> res = null;
	try{
		
		tx = session.beginTransaction();
		Query query = session.createQuery(" FROM Flight ");		
		res = query.list();
		tx.commit();
		
	}catch(HibernateException e){
		
		System.out.println("Get all flights :"+e);
		if (tx != null)
            tx.rollback();
		
	}finally{
		session.close();
	}
	
	return res;
	
	}
	
	public void AddFlight(Flight flight){
		
		Session session = factory.openSession();
		Transaction tx = null;
		
		try{
			
			tx = session.beginTransaction();
			session.save(flight);
			tx.commit();
			
		}catch(HibernateException e){
			
			System.out.println("Add flight :"+e);
			if (tx != null)
				tx.rollback();
			
		}finally{
		session.close();
		}
			
		
	}
	
	public void deleteFlights(ArrayList<String> flightNumberToDelete){
		Session session = factory.openSession();
		Transaction tx = null;
		
		try{
			for (String flightToDelete: flightNumberToDelete)
            {
				tx = session.beginTransaction();
				Query query = session.createQuery("delete Flight where flightNr =:flightToDel");
				query.setParameter("flightToDel", flightToDelete);
				query.executeUpdate();
				tx.commit();
            }
		}catch(HibernateException e){
			
			System.out.println("Delete flights :"+e);
			if (tx != null)
				tx.rollback();
			
		}finally{
		session.close();
		}
	}

}
