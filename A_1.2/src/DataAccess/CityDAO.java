package DataAccess;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import Entities.City;

public class CityDAO {
	
	private SessionFactory factory;
	
	public CityDAO(SessionFactory factory){
		this.factory = factory;
	}
	
	public List<City> getAllCities(){
		
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> res = null;
		
		try{
			
			tx = session.beginTransaction();
			Query query = session.createQuery(" FROM City ");	
			res = query.list();
			tx.commit();
			
		}catch(HibernateException e){
			
			System.out.println("Get all cities :"+e);
			if (tx != null)
	            tx.rollback();
			
		}finally{
			session.close();
		}
		
		return res;
		
	}

}
