package DataAccess;

import Entities.User;

import java.util.List;

import org.hibernate.*;


public class LoginDAO {
	
	private SessionFactory factory;
	

	public LoginDAO(SessionFactory factory) {
		
		this.factory = factory;
		
	}
	
	public User check(User user){

		Session session = factory.openSession();
		Transaction tx = null;
		User returnedUser = null;
		
        try
        {

		tx=session.beginTransaction();
		Query q1 = session.createQuery("SELECT username, password, type FROM User WHERE username =:user_name AND password =:pass");
		
		q1.setParameter("user_name", user.getUsername());
		q1.setParameter("pass", user.getPassword());
		
		List<Object[]> result = (List<Object[]>)q1.list();
		for( Object[] usr: result)
        {
            String username = (String) usr[0];
            String password = (String) usr[1];
            String type     = (String) usr[2];

            returnedUser = new User();
            returnedUser.setUsername(username);
            returnedUser.setPassword(password);
            returnedUser.setType(type);
        }
        tx.commit();
        }catch (HibernateException e)
        {
            System.out.println("Check user error" + e);
            if (tx != null)
                tx.rollback();
        }
        finally
        {
            session.close();
        }
        return  returnedUser;
    }

	
}
