package Presentation;

import java.util.List;

import BusinessLogic.NormalUserBO;
import BusinessLogic.UserBO;
import Entities.City;
import Entities.Flight;

public class UserView extends View{
	
	private NormalUserBO normalUserBO = null; 
	
	public UserView(){
		
		normalUserBO = new NormalUserBO();
		
	}

	@Override
	public String LoadPage(UserBO userBO) {

		String returnedFlight = null;
		String returnedCity = null;
		String flightToCheckbox = "";
		String cityToCombobox = "";
		String title = "NORMAL USER";
		
		
		if (userBO == null){
			return null;
		}
		
		
		List <Flight> flights = normalUserBO.getAllFlights();
		List <City> cities    = normalUserBO.getAllCities();
		
		for(Flight flight: flights )
        {
            returnedFlight =  flight.toString();
            String inputCheckbox = "<input type=\"checkbox\" disabled =\"disabled\" "
            		+ "		 		name=\"flights\" "
            		+ "		  		value=\""+returnedFlight+ "\" "
            		+ "  	  		checked=\"checked\">" + returnedFlight + " <br>";
            flightToCheckbox += inputCheckbox;
            
        }
		
		for(City city: cities){
			returnedCity = city.toString();
			String inputCombobox = "<option value=" + returnedCity + ">" + returnedCity + "</option>";
			cityToCombobox +=inputCombobox;
		}
		
		
		
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
		String content = ( docType +
				"<html>\n" +
                "<head>" +
                "<title>" + title + "</title>" +
                	"</head>\n" +
                		"<body bgcolor=\"#f0f0f0\">\n" +
                			"<h1 align=\"center\">" + title + "</h1>\n" +
                			"<form = action= \"removeSelectedFlights\"  method=\"post\" target=\"_blank\">" +
                				flightToCheckbox  +
                			" </form>" +
                			"<form = action= \"queryCities\" method=\"get\" target=\"_blank\">" +
                				"<select>" +
                			    	"cityToCombobox" +
                			    "</select>" +
                			    "<input type=\"submit\" value=\"Query on city\" name=\"submit3\" />" +
                			" </form>" +
                			    
                		 		"</body>" +
                                "</html>"
                                  );
		
		
		return content;
	}
	
	
	
	

}
