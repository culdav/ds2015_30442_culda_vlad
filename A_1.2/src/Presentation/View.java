package Presentation;

import BusinessLogic.AdminBO;
import BusinessLogic.UserBO;

public class View {
	
	private View view = null;
    private UserBO userBO = null;
    
    public View()
    {
    }

    public View(String username, String password)
    {
        try
        {
        	UserBO UserBO = new UserBO();
        	this.userBO = UserBO.CheckUser(username, password);
        	if (this.userBO != null)
        		{
        			if (this.userBO != null && this.userBO.getClass().getName().equals(AdminBO.class.getName()))
        			{
        				this.view = new AdminView();
        			}
        			else
        			{
        				this.view = new UserView();
        			}
        		}
        }catch (Exception e){
        System.out.println("Error" + e);
        }

     }
    
    public String Init()
    {
        if (userBO != null)
           return  view.LoadPage(userBO);
        else
            return "Check username and password";

    }
    
    public UserBO getUserBO()
    {
        return userBO;
    }
    
    public String LoadPage(UserBO userBO)
    {
        String content = "Simple user";
        return content;
    }
    
    
    
    
    
    


}
