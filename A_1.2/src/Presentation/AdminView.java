package Presentation;

import java.util.ArrayList;
import java.util.List;

import BusinessLogic.AdminBO;
import BusinessLogic.FlightBO;
import BusinessLogic.UserBO;
import Entities.Flight;

public class AdminView extends View{
	
	private AdminBO adminBO = null;
	
	public AdminView(){
		adminBO = new AdminBO();
	}

	@Override
	public String LoadPage(UserBO userBO) {
		
		String flightToCheckbox = "";
		String returnedFlight = null;
		String title = "ADMIN";
		
		if (userBO == null){
			return null;
		}
		
		
		List <Flight> flights = adminBO.getAllFlights();
		
		for(Flight flight: flights )
        {
            returnedFlight =  flight.toString();
            String inputCheckbox = "<input type=\"checkbox\" "
            		+ "		 		name=\"flights\" "
            		+ "		  		value=\""+returnedFlight+ "\" "
            		+ "  	  		checked=\"checked\">" + returnedFlight + " <br>";
            flightToCheckbox += inputCheckbox;
        }
		
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
		String content = ( docType +
				"<html>\n" +
                "<head>" +
                "<title>" + title + "</title>" +
                	"</head>\n" +
                		"<body bgcolor=\"#f0f0f0\">\n" +
                			"<h1 align=\"center\">" + title + "</h1>\n" +
                			"<form = action= \"removeSelectedFlights\"  method=\"post\" target=\"_blank\">" +
                				flightToCheckbox  +
                			 "<input type=\"submit\" value=\"Delete Flight\" name=\"submit1\" />" +
                			"</form>" +
                			
                			
                			"<form method = \"post\" action = \"addNewFlight\"> " + 
                			"Flight Number:  <input type=\"text\" name=\"flightNr\" > <br> " + 
                			"Airplane Type:  <input type=\"text\" name=\"airplaneType\" > <br> " +
                			"Departure city: <input type=\"text\" name=\"departCity\" >  <br> " +
                			"Departure date: <input type=\"date\" name=\"departDate\" > <br> " +
                			"Departure time: <input type=\"text\" name=\"departTime\" >  <br> " +
                			"Arrival city:   <input type=\"text\" name=\"arrivalCity\" >  <br> " +
                			"Arrival date:   <input type=\"date\" name=\"arrivalDate\" >  <br> " +
                			"Arrival time:   <input type=\"text\" name=\"arrivalHour\" >  <br> " +
                		    "<input type=\"submit\" value=\"Add Flight\" name=\"submit2\" />" +
                			"</form>"+
                		    
                		"</body>" +
                "</html>"
                  );
						
		
		return content;
		
	}
	
	public void AddFlight(Flight flight){
		
		adminBO.addFlight(flight);
		
		
	}
	
	public void deleteFlights(ArrayList<String> flightsNumberToDelete){
		
		adminBO.deleteFlights(flightsNumberToDelete);
		
		
	}
	
	public void reload_page(){
		
	}
	
	
	
	

}
