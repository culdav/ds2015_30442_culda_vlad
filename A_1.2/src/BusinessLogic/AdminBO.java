package BusinessLogic;

import java.util.ArrayList;
import java.util.List;

import Entities.Flight;
import Entities.User;

public class AdminBO extends UserBO{
	
	public User user = null;
	
	public AdminBO(User user){
		
		this.user = user;
	}
	
	public AdminBO() {
		// TODO Auto-generated constructor stub
	}

	public AdminBO getType(){
		
		if(this.user == null || !this.user.getType().equals("admin")){
			return null;
		}
		
		return this;
	}
	
	public List<Flight> getAllFlights(){
		
		FlightBO flightBO =  new FlightBO();
		return flightBO.getAll();
		
	}
	
	public void addFlight(Flight flight){
		FlightBO flightBO = new FlightBO();
		flightBO.AddFlight(flight);
	}
	
	public void deleteFlights(ArrayList<String> flightsNumberToDelete){
		FlightBO flightBO = new FlightBO();
		flightBO.deleteFlights(flightsNumberToDelete);
	}

}