package BusinessLogic;

import org.hibernate.cfg.Configuration;

import DataAccess.LoginDAO;
import Entities.User;

public class UserBO{
	
	private User user = null;
	
    public User getUser() {
	        return user;
	    }
	
    public UserBO CheckUser(String username, String password) {
        
    	User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        
        @SuppressWarnings("deprecation")
		LoginDAO usersDAO = new LoginDAO(new Configuration().configure().buildSessionFactory());
        
        User returnedUser = usersDAO.check(user);
        
        if(returnedUser != null){
        	UserBO userBO = null;
        	this.user = returnedUser;
        	AdminBO adminBO = new AdminBO(this.user);
        	adminBO = adminBO.getType();
        	
        	NormalUserBO normalUserBO = new NormalUserBO(this.user);
        	normalUserBO = normalUserBO.getType();
        	
        	if(adminBO != null){
        		userBO = adminBO;
        	}
        	else {
        		
        		userBO = normalUserBO;
        		
        		}
			
        	return userBO;
        }
        
        return null;
    }
}