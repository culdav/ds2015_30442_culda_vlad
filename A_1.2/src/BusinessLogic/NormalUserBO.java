package BusinessLogic;

import java.util.List;

import Entities.City;
import Entities.Flight;
import Entities.User;

public class NormalUserBO extends UserBO{
	
	public User user = null;
	
	public NormalUserBO(User user){
		
		this.user = user;
	}
	
	public NormalUserBO() {
		// TODO Auto-generated constructor stub
	}

	public NormalUserBO getType(){
		
		if(this.user == null || !this.user.getType().equals("user")){
			return null;
		}
		
		return this;
	}

	public List<Flight> getAllFlights() {
		
		FlightBO flightBO =  new FlightBO();
		return flightBO.getAll();
	}

	public List<City> getAllCities() {
		
		CityBO cityBO = new CityBO();
		return cityBO.getAllCities();
	}

}