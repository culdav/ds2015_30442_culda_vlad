package BusinessLogic;

import java.util.List;

import org.hibernate.cfg.Configuration;

import DataAccess.CityDAO;
import Entities.City;

public class CityBO {
	
	public CityBO(){
		
	}

	public List<City> getAllCities() {
		// TODO Auto-generated method stub
		CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
		return cityDAO.getAllCities();
	}

}
