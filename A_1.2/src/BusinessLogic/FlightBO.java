package BusinessLogic;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.cfg.Configuration;

import DataAccess.FlightDAO;
import Entities.Flight;

public class FlightBO {

	public FlightBO(){
		
	}
	
	
	public List<Flight> getAll(){
		
		@SuppressWarnings("deprecation")
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		
		return flightDAO.getAll();
	
	}
	
	public void AddFlight(Flight flight){
		
	   FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
	   flightDAO.AddFlight(flight);
		
	}
	
	public void deleteFlights(ArrayList<String> flightNumberToDelete){
		
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		flightDAO.deleteFlights(flightNumberToDelete);
		
	}
	
}
