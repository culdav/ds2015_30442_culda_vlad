package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;

public class PriceService implements IPriceService{

	@Override
	public double computeSellingPrice(Car c) {
		
		double sellingPrice = 0;
		
		if (c.getPrice() <= 0) {
			throw new IllegalArgumentException("Price of the car must be positive.");
		}

		sellingPrice = c.getPrice() - (c.getPrice()/7) * (2015 - c.getYear());
		
		return sellingPrice;
		
	}
	
	
	
	
}
