package Logic;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.interfaces.Constants;
import com.interfaces.IPriceService;
import com.interfaces.ITaxService;

import Communication.RMIClient;
import Entities.Car;
import GUI.GUI;

public class ClientLogic {
	
	private GUI clientGUI;
	private RMIClient rmiClient;
	
	public ClientLogic(GUI clientGUI, RMIClient rmiClient){
		
		this.clientGUI = clientGUI;
		this.rmiClient = rmiClient;
		
	}
	
	public static double computeTax(String year, String engineCapac, String price) throws MalformedURLException, RemoteException, NotBoundException{
		
		Car c = new Car(Integer.parseInt(year),Integer.parseInt(engineCapac),Double.parseDouble(price));
		Registry registry = LocateRegistry.getRegistry("localhost",Constants.RMI_PORT);
		
		ITaxService iTaxService = (ITaxService) registry.lookup(Constants.RMI_ID_TAXPRICE);
		
		double tax = iTaxService.computeTax(c);
		
		return tax;
		
		
	}
	
	public static double computePrice(String year, String engineCapac, String price) throws RemoteException, MalformedURLException, NotBoundException{
		
	   Car c = new Car(Integer.parseInt(year),Integer.parseInt(engineCapac),Double.parseDouble(price));
	   Registry registry = LocateRegistry.getRegistry("localhost",Constants.RMI_PORT);
	   IPriceService iPriceSevice = (IPriceService) Naming.lookup(Constants.RMI_ID_PRICESERV);
	   
	   double sellingPrice = iPriceSevice.computeSellingPrice(c);
	   
	   return sellingPrice;
		
	}
	
	
}
