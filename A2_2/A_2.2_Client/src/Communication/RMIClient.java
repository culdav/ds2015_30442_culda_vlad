package Communication;

import GUI.GUI;
import Logic.ClientLogic;

public class RMIClient {
	
	public static void main(String[] args) {
	
		GUI clientGUI = new GUI();
		RMIClient rmiClient = new RMIClient();
		ClientLogic logic = new ClientLogic(clientGUI, rmiClient);
		clientGUI.setVisible(true);
		
	}
	
}
