package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Logic.ClientLogic;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField yearTextField;
	private JTextField priceTextField;
	private JTextField capacTextField;
	private JLabel taxLabel;
	private JLabel priceLabel;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public GUI() {
		
		setTitle("Tax calculator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		yearTextField = new JTextField();
		yearTextField.setBounds(83, 21, 86, 20);
		contentPane.add(yearTextField);
		yearTextField.setColumns(10);
		
		priceTextField = new JTextField();
		priceTextField.setBounds(83, 52, 86, 20);
		contentPane.add(priceTextField);
		priceTextField.setColumns(10);
		
		capacTextField = new JTextField();
		capacTextField.setBounds(83, 83, 86, 20);
		contentPane.add(capacTextField);
		capacTextField.setColumns(10);
		
		JButton taxBtn = new JButton("Tax");
		taxBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			try {
				taxLabel.setText("" + ClientLogic.computeTax(yearTextField.getText(), capacTextField.getText(), priceTextField.getText()));
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NotBoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
			}
		});
		taxBtn.setBounds(220, 32, 89, 23);
		contentPane.add(taxBtn);
		
		JButton priceBtn = new JButton("Sell Price");
		priceBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					priceLabel.setText("" + ClientLogic.computePrice(yearTextField.getText(), capacTextField.getText(), priceTextField.getText()));
				} catch (MalformedURLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		priceBtn.setBounds(220, 66, 89, 23);
		contentPane.add(priceBtn);
		
		JLabel lblNewLabel = new JLabel("Year:");
		lblNewLabel.setBounds(10, 24, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Init. price:");
		lblNewLabel_1.setBounds(10, 58, 63, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Capacity:");
		lblNewLabel_2.setBounds(10, 89, 63, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblCmc = new JLabel("cmc");
		lblCmc.setBounds(176, 86, 46, 14);
		contentPane.add(lblCmc);
		
		JLabel lblRon = new JLabel("RON");
		lblRon.setBounds(176, 55, 46, 14);
		contentPane.add(lblRon);
		
		taxLabel = new JLabel("");
		taxLabel.setBounds(319, 36, 96, 14);
		contentPane.add(taxLabel);
		
		priceLabel = new JLabel("");
		priceLabel.setBounds(329, 70, 86, 14);
		contentPane.add(priceLabel);
	}
}
