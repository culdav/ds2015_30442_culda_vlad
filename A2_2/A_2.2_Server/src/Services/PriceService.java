package Services;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import com.interfaces.IPriceService;

import Entities.Car;

public class PriceService extends UnicastRemoteObject implements IPriceService ,Serializable {

	private static final long serialVersionUID = 3027969188548691805L;

	public PriceService() throws RemoteException {
		super();
	}

	@Override
	public double computeSellingPrice(Car c) throws RemoteException {
		
		double sellingPrice = 0;
		
		if (c.getPrice() <= 0) {
			throw new IllegalArgumentException("Price of the car must be positive.");
		}

		sellingPrice = c.getPrice() - (c.getPrice()/7) * (2015 - c.getYear());
		
		return sellingPrice;

	}
	
	
}
