package Communication;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.interfaces.Constants;
import com.interfaces.IPriceService;
import com.interfaces.ITaxService;

import Services.PriceService;
import Services.TaxService;

public class RMIServer {
	
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		
		System.out.println("Server started");
		
		Registry registry = LocateRegistry.createRegistry(1099);
		
		ITaxService taxService = new TaxService();
		registry.bind(Constants.RMI_ID_TAXPRICE, taxService);
		
		IPriceService priceService = new PriceService();
		registry.bind(Constants.RMI_ID_PRICESERV, priceService);
		
		while (true) {
			try {
				Thread.currentThread().sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
