package com.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

import Entities.Car;

public interface IPriceService extends Remote{
	
	double computeSellingPrice(Car c) throws RemoteException;
	
}
