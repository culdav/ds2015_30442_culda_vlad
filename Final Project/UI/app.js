(function () {
    'use strict';
    

    
    angular
        .module('app', ['ngRoute', 'ngCookies'])
        .config(config)
        .run(run);
    
    
    config.$inject = ['$routeProvider', '$locationProvider','$httpProvider'];
    
    function config($routeProvider, $locationProvider, $httpProvider) {
        $routeProvider
            .when('/moderator',{
                controller: 'ModeratorController',
                templateUrl: 'moderator/moderator.view.html',
                controllerAs: 'vm'
            })
            .when('/home', {
                controller: 'HomeController',
                templateUrl: 'home/home.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'login/login.view.html',
                controllerAs: 'vm'
            })

            .when('/addUser', {
                controller: 'AddUserController',
                templateUrl: 'register/addUser.view.html',
                controllerAs: 'vm'
            })
            .when('/viewStudent', {
                controller: 'StudentDetailsController',
                templateUrl: 'home/studentDetails.view.html',
                controllerAs: 'vm'
            })
            .when('/addSubject', {
                controller: 'AddSubjectController',
                templateUrl: 'register/addSubject.view.html',
                controllerAs: 'vm'
            })
            
            .otherwise({ redirectTo: '/login'
                      });
        
       
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
    function run($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if($location.path() !== '/viewStudent') {
                if( $location.path() !== '/addSubject') {
                    if( $location.path() !== '/addUser'){
                        if (restrictedPage && !loggedIn) {
                            $location.path('/login');
                        }
                    }
                }
            } 
        });
    }
    
  
})();