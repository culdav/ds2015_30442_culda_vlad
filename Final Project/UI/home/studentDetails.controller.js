(function () {
    'use strict';

    angular
        .module('app')
        .controller('StudentDetailsController', StudentDetailsController);

    StudentDetailsController.$inject = ['UserService', '$rootScope'];
    function StudentDetailsController(UserService, $rootScope) {
        var vm = this;
        
        vm.user = null;
        
        initCtrl();
        
        function initCtrl(){
            loadUser();
            
        }
      
        function loadUser() {
            UserService.GetById($rootScope.globals.selectedUser.id)
                .then(function (user) {
                    vm.user = user;
                    console.log(vm.user);
                    
                });
        }
        
        function ShowUser(){
            
        }
          
    }

})();
