(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);
        

    
    HomeController.$inject = ['UserService','SubjectService', '$rootScope', '$location'];
    function HomeController(UserService, SubjectService, $rootScope, $location) {
       
       var vm = this;

       vm.user = null;
       vm.allUsers = [];
       vm.deleteUser = deleteUser;
       vm.loadSelectedUserId1 = loadSelectedUserId1;
       vm.loadSelectedUserId2 = loadSelectedUserId2;
       vm.addGradeClick = addGradeClick;
       vm.allSubjects = [];
        
     //  var teacherSubject;
     
        
        initController();

        function initController() {
            loadCurrentUser();
            loadAllUsers();
            loadAllSubjects();
  
            console.log(vm.user);
            console.log(vm.allUsers);
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                //set global variable in order to be used on the next page
                   $rootScope.globals = {
                     teacher: {
                        id: user.id,
                        name: user.name,
                        username: user.username,
                        password: user.password,
                        phone: user.phone,
                        type: user.type,
                        mail: user.mail,
                        grades: user.grades
                 }
             }
                });
        }

        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
            .then(function () {
                loadAllUsers();
            });
        }
        
        function loadSelectedUserId1() {
        
            //set global variable in order to be used on the next page
            $rootScope.globals = {
                 selectedUser: {
                    id: vm.id
                 }
             }
             //set path to viewStudentDetails html
             $location.path("/viewStudent");
                        
        }
        
         function loadSelectedUserId2() {
        
            //set global variable in order to be used on the next page
            $rootScope.globals = {
                 selectedUser: {
                    id: vm.id
                 }
             }
             
                        
        }
        
        function loadAllSubjects(){
            
            SubjectService.GetAll()
                .then(function(subjects) {
                    vm.allSubjects = subjects;
            });
      
        }
        
        function getTeacherSubject(){
            var sbj;
            vm.allSubjects.forEach(function(subject){
                
                if(subject.teacher.username === vm.user.username){
                    sbj = subject;
                   
                }
                
            });
                    
            return sbj;
        }
        
        function addGrade() {
            var grade; 
            var gradeToAdd;
            var subject = getTeacherSubject();
            var subjectToAdd = {
                name: subject.name,
                teacher: {
                    username: subject.teacher.username,
                    password: subject.teacher.password,
                    type: subject.teacher.type,
                    name: subject.teacher.name,
                    mail: subject.teacher.mail,
                    phone: subject.teacher.phone,
                    grades: subject.teacher.graeds  
                }
            };
            
            gradeToAdd = {            
                grade: vm.grade,
                subject: subjectToAdd   
            }
            
            UserService.AddGrade(vm.id, gradeToAdd)
            .then(function(response) {
                if(response)
                    $location.path('/viewStudent');
            })
            
            
        }
        
        function addGradeClick(){
            
            loadSelectedUserId2();
            addGrade();
            
        }
        
    }

})();