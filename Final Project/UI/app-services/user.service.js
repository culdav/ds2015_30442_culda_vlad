(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.AddUser = AddUser;
        service.Update = Update;
        service.Delete = Delete;
        service.GetGrades = GetGrades;
        service.GetGradesByUsername = GetGradesByUsername;
        service.AddGrade = AddGrade;
       

        return service;

        function GetAll() {
            
             return $http.get('http://localhost:8080/university/user/all').then(
                 handleSuccess, handleError('Error getting all users'));
            
        }

        function GetById(id) {
            //return $http.get('http://localhost:8080/university/user/id' + id).then(handleSuccess(res), handleError('Error getting user by id'));   
            return $.ajax({
		      dataType: 'json',
		      url: 'http://localhost:8080/university/user/id/' + id,
              crossDomain: true,
              type: 'GET',
              success: function(data) {
                    handleSuccess(data); 
              },
              error:  handleError('Error getting all users')
            });
            
        }

        function GetByUsername(username) {
                       
            return $.ajax({
		      dataType: 'json',
		      url: 'http://localhost:8080/university/user/username/' + username,
              crossDomain: true,
              type: 'GET',
              success: function(data) {
                    handleSuccess(data); 
              },
              error:  handleError('Error getting all users')
            });
        }

        function AddUser(user) {
            var res;
            var data; 
            return $http.post('http://localhost:8080/university/user/add', user).then(
                 res = {
                     data: {
                            success: true
                           }
                 },
                handleSuccess(res), handleError('Error creating user'));
            
        }

        function Update(user) {
            var res;
            return $http.put('/api/users/' + user.id, user).then( 
                 res = {
                  success: true
                 },
                handleSuccess(res), handleError('Error updating user'));
        }

        function Delete(id) {
           // return $http.delete('http://localhost:8080/university/delete/' + id).then(handleSuccess, handleError('Error deleting user'));
            return $.ajax({
		      //dataType: 'json',
		      url: 'http://localhost:8080/university/user/delete/' + id,
              crossDomain: true,
              type: 'DELETE'
             // success: $route.reload(),
             // error:  handleError('Error deleting user ')
            });
        }
        
        function GetGrades(id){
            return $.ajax({
		      dataType: 'json',
		      url: 'http://localhost:8080/university/grade/all/' + id,
              crossDomain: true,
              type: 'GET',
              success: function(data) {
                    handleSuccess(data); 
              },
              error:  handleError('Error getting all users')
            });
        }
        
        function GetGradesByUsername(username){
            return $.ajax({
		      dataType: 'json',
		      url: 'http://localhost:8080/university/grade/username/' + username,
              crossDomain: true,
              type: 'GET',
              success: function(data) {
                    handleSuccess(data); 
              },
              error:  handleError('Error getting all users')
            });
        }
        
        function AddGrade(userId,grade){
            
            var res;
            var data; 
            return $http.post('http://localhost:8080/university/user/addGrade/' + userId, grade).then(
                 res = {
                     data: {
                            success: true
                           }
                 },
                handleSuccess(res), handleError('Error adding grade to a user'));
            
        }
        
        
        

        // private functions
        
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
