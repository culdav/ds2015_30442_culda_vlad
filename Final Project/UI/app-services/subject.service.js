(function () {
    'use strict';

    angular
        .module('app')
        .factory('SubjectService', SubjectService);

    SubjectService.$inject = ['$http'];
    function SubjectService($http) {
        var service = {};

        service.GetAll = GetAll;
       // service.GetById = GetById;
        service.AddSubject = AddSubject;
       // service.Update = Update;
        service.DeleteSubject = DeleteSubject;
    
        return service;

        function GetAll() {
            
             return $http.get('http://localhost:8080/university/subject/all').then(
                 handleSuccess, handleError('Error getting all subjects'));
            
        }

    

        function AddSubject(subject) {
            var res;
            var data; 
            return $http.post('http://localhost:8080/university/subject/add', subject).then(
                 res = {
                     data: {
                            success: true
                           }
                 },
                handleSuccess(res), handleError('Error creating subject'));
            
        }

        function DeleteSubject(id) {
           // return $http.delete('http://localhost:8080/university/delete/' + id).then(handleSuccess, handleError('Error deleting user'));
            return $.ajax({
		      //dataType: 'json',
		      url: 'http://localhost:8080/university/subject/delete/' + id,
              crossDomain: true,
              type: 'DELETE'
             // success: $route.reload(),
             // error:  handleError('Error deleting user ')
            });
        }
        
        // private functions
        
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
