(function () {
    'use strict';

    angular
        .module('app')
        .controller('ModeratorController', ModeratorController);

    ModeratorController.$inject = ['UserService', '$rootScope', '$location' ,'$route'];
    function ModeratorController(UserService, $rootScope, $location , $route) {
       
        var vm = this;
        
        var userId;
        vm.user;
        vm.allGrades;
        
      
        initializeController();
        
        
        function initializeController() {
            loadCurrentUser();
            loadAllGrades();
            console.log(vm.user);
            console.log(vm.allGrades);
               

        }
        
        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
               .then(function (user) {
                   vm.user = user;
               console.log(vm.user);
                    
                });
            
         
            
        }
        
        function loadAllGrades(){
   
            UserService.GetGradesByUsername($rootScope.globals.currentUser.username)
                .then(function(grades){
                    vm.allGrades = grades;
                    console.log(vm.allGrades);
            });
            
            
        }   
        
        function showUser(){
            
        }
        
    }

})();