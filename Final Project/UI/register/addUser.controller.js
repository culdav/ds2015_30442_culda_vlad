(function () {
    'use strict';

    angular
        .module('app')
        .controller('AddUserController', AddUserController);

    AddUserController.$inject = ['UserService', '$location', '$rootScope', 'FlashService'];
    function AddUserController(UserService, $location, $rootScope, FlashService) {
        var vm = this;

        vm.AddUser = register;

        function register() {
            vm.dataLoading = true;
            UserService.AddUser(vm.user)
                .then(function (response) {
                    if (response.statusText === "Created") {
                        FlashService.Success('User added successfully', true); 
                        $location.path('/home');
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
        }
    }

})();
