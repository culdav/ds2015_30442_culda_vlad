(function () {
    'use strict';

    angular
        .module('app')
        .controller('AddSubjectController', AddSubjectController);

    AddSubjectController.$inject = ['SubjectService', '$location', '$rootScope', 'FlashService'];
    function AddSubjectController(SubjectService, $location, $rootScope, FlashService) {
        var vm = this;
        var subjectToCreate;
        var teacher;
        vm.AddSubject = AddSubject;
        vm.allSubjects = [];
        
        initController();
        
        function initController(){
            GetAllSubjects();
        }
        
        function AddSubject() {
            vm.dataLoading = true;
            // get the credentials of the logged in professor
            subjectToCreate = {
                name: vm.subject.name,
                teacher: {
                    username : $rootScope.globals.teacher.username,
                    name : $rootScope.globals.teacher.name,
                    password : $rootScope.globals.teacher.password,
                    mail : $rootScope.globals.teacher.mail,
                    phone : $rootScope.globals.teacher.phone,
                    grades : $rootScope.globals.teacher.grades,
                    type : $rootScope.globals.teacher.type,
                    name : vm.subject.name
                }
            }
        
            SubjectService.AddSubject(subjectToCreate)
                .then(function (response) {
                    if (response.statusText === "Created") {
                        FlashService.Success('Subject added successfully', true); 
                        $location.path('/addSubject');
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
        }
        
        
        function GetAllSubjects(){
            
            SubjectService.GetAll()
            .then(function(subjects){
                vm.allSubjects = subjects;
            });
        }
    }

})();
