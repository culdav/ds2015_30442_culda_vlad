package com.university.service;

import java.util.List;


import com.university.model.Subject;

public interface SubjectService {
	
	public void addSubject(Subject subject);
	public void updateSubject(Subject subject);
	public Subject getSubject(int subjectId);
	public void deleteSubject(int subjectId);
	public List<Subject> getSubjects();
	

}
