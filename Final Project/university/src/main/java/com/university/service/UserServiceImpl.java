package com.university.service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.university.dao.UserDAO;
import com.university.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDAO;

	public List<User> getUsers() {
		return userDAO.getUsers();
	}

	public User getUser(String username, String password) {
		
		return userDAO.getUser(username, password);
	}

	public User getByUsername(String username) {
		
		return userDAO.getByUsername(username);
		
	}

	@Override
	public void deleteUser(int id) {
		
		userDAO.deleteUser(id);
		
	}

	@Override
	public User getUserById(int id) {
		
		return userDAO.getUserById(id);
	}

	@Override
	public void createUser(User user) {
		
		userDAO.createUser(user);
		
	}

	@Override
	public void updateUser(User user) {
		
		userDAO.updateUser(user);
		
	}

}
