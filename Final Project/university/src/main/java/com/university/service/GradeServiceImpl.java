package com.university.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.university.dao.GradeDAO;

import com.university.model.Grade;

@Service
@Transactional
public class GradeServiceImpl implements GradeService{
	
	
	@Autowired
	private GradeDAO gradeDAO;
	
	@Override
	public void addGrade(Grade grade) {
	
	gradeDAO.addGrade(grade);
	
	}
		
	
	
	
	
	
}
