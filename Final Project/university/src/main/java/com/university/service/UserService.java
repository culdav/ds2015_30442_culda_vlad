package com.university.service;

import java.util.List;

import com.university.model.User;

public interface UserService {
	
	public List<User> getUsers();
	public User getUser(String username,String password);
	public User getByUsername(String username);
	public void deleteUser(int id);
	public User getUserById(int id);
	public void createUser(User user);
	public void updateUser(User user);

}
