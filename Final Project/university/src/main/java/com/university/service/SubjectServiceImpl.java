package com.university.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.university.dao.SubjectDAO;
import com.university.model.Subject;

@Service
@Transactional
public class SubjectServiceImpl implements SubjectService{
	
	@Autowired
	private SubjectDAO subjectDAO;

	public void addSubject(Subject subject) {
		
		subjectDAO.addSubject(subject);
		
	}

	public void updateSubject(Subject subject) {
		
		subjectDAO.updateSubject(subject);
		
	}

	public Subject getSubject(int subjectId) {
		
		return subjectDAO.getSubject(subjectId);
		
	}

	public void deleteSubject(int subjectId) {

		subjectDAO.deleteSubject(subjectId);;
		
	}

	public List<Subject> getSubjects() {
		
		return subjectDAO.getAllSubjects();
		
	}

	
	
	
	
	
	
	
	
}
