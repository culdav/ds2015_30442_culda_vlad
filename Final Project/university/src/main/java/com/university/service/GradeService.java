package com.university.service;

import com.university.model.Grade;

public interface GradeService {
	
	public void addGrade(Grade grade);
	
}
