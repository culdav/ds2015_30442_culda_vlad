package com.university.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.university.model.Grade;
import com.university.model.User;
import com.university.service.GradeService;
import com.university.service.UserService;

@RestController
@RequestMapping(value="/grade")
public class GradeController {
	
	
	@Autowired
	private UserService userService;
	
	@Autowired GradeService gradeService;
	
	 @RequestMapping(value = "/all/{id}", method = RequestMethod.GET)
	    public ResponseEntity <Collection<Grade>> getGradesById(@PathVariable String id) {
	       	
		 	int userId = Integer.parseInt(id);
		 	User user = userService.getUserById(userId);
	        
	        if(user == null){
	           // return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	        }
	        //return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	        Collection<Grade> allGrades = user.getGrades();
	        return new ResponseEntity<Collection<Grade>>(allGrades,HttpStatus.OK);
	    }
	 
	 @RequestMapping(value = "/username/{name}", method = RequestMethod.GET)
	    public ResponseEntity <Collection<Grade>> getGradesByUsername(@PathVariable String name) {
	       	
		 	
		 	User user = userService.getByUsername(name);
	        
	        if(user == null){
	            return new ResponseEntity<Collection<Grade>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
	        }
	        //return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	        Collection<Grade> allGrades = user.getGrades();
	        return new ResponseEntity<Collection<Grade>>(allGrades,HttpStatus.OK);
	    }
	 
	 
	
}
