package com.university.controller;

import java.util.Collection;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.university.model.Grade;
import com.university.model.Subject;
import com.university.model.User;
import com.university.service.UserService;

@RestController
@RequestMapping(value="/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	//-------------------Retrieve All Users--------------------------------------------------------
    
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity <Collection<User>> listAllUsers() {
        List<User> users = userService.getUsers();
        
        if(users.isEmpty()){
           // return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        //return new ResponseEntity<List<User>>(users, HttpStatus.OK);
        return new ResponseEntity<Collection<User>>(users,HttpStatus.OK);
    }
    
//    @RequestMapping(value = "/param/",params = { "username", "password" }, method = RequestMethod.GET)
//	public @ResponseBody User getUser(@RequestParam(value = "username") String username, @RequestParam(value = "password") String password){
//
//    	User user = userService.getUser(username, password);
//    	 //return new ResponseEntity<User>(user,HttpStatus.OK);
//    	return user;
//    }
    
    @RequestMapping(value = "/param/{username},{password}", method = RequestMethod.GET)
	public @ResponseBody User getUser(@PathVariable String username, @PathVariable String password){

    	User user = userService.getUser(username, password);
    	 //return new ResponseEntity<User>(user,HttpStatus.OK);
    	return user;
    }
    
//    @RequestMapping(value = "/username/",params = { "username" }, method = RequestMethod.GET)
//   	public ResponseEntity <User> getUser(@RequestParam(value = "username") String username){
//       	User user = userService.getByUsername(username);
//       	 return new ResponseEntity<User>(user,HttpStatus.OK);
//       }
      
    @RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
   	public ResponseEntity <User> getUser(@PathVariable String username){
       	User user = userService.getByUsername(username);
       	 return new ResponseEntity<User>(user,HttpStatus.OK);
       }
    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable int id){
    	User user = userService.getUserById(id);
    	if (user == null) {
            System.out.println("Unable to delete. User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
    	
    	userService.deleteUser(id);
    	return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    	
    	
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder ucBuilder){
    	
    	userService.createUser(user);
    	
    	 HttpHeaders headers = new HttpHeaders();
	     headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getUser_id()).toUri());
	     return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	
    }
    
    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
   	public ResponseEntity <User> getUserById(@PathVariable int id){
       	User user = userService.getUserById(id);
       	 return new ResponseEntity<User>(user,HttpStatus.OK);
       }
    
    @RequestMapping(value = "/addGrade/{studentId}", method = RequestMethod.POST)
 	public ResponseEntity <Void> addGrade(@RequestBody Grade newGrade,@PathVariable int studentId)
 {
	 User user =  userService.getUserById(studentId);
	 
	 Collection<Grade> allGrades = user.getGrades();

	 allGrades.add(newGrade);
	 
	 userService.updateUser(user);
	 
	 //HttpHeaders headers = new HttpHeaders();
     //headers.setLocation(ucBuilder.path("/all/{id}").buildAndExpand(newGrade.getGrade_id()).toUri());
     return new ResponseEntity<Void>(HttpStatus.CREATED);
	 
 }
     
}
