package com.university.controller;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;


import com.university.model.Subject;
import com.university.service.SubjectService;


@RestController
@RequestMapping(value="/subject")
public class SubjectController {
	
	@Autowired
	private SubjectService subjectService;
	
//-------------------Retrieve All Subjects--------------------------------------------------------
    
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity <Collection<Subject>> listAllSubjects() {
        List<Subject> subjects = subjectService.getSubjects();
        
        if(subjects.isEmpty()){
            return new ResponseEntity<Collection<Subject>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        
        return new ResponseEntity<Collection<Subject>>(subjects,HttpStatus.OK);
    }
    
//-------------------Retrieve Subject By ID--------------------------------------------------------    
    @RequestMapping(value="/get/{id}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Subject> getSubject(@PathVariable("id") int id) {
        System.out.println("Fetching Subject with id " + id);
        Subject subject = subjectService.getSubject(id);
        if (subject == null) {
            System.out.println("student with id " + id + " not found");
            return new ResponseEntity<Subject>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Subject>(subject, HttpStatus.OK);
    }
  
  //-------------------Add a new subject--------------------------------------------------------  
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Void> createSubject(@RequestBody Subject subject, UriComponentsBuilder ucBuilder){
    	
    	subjectService.addSubject(subject);
    	
    	 HttpHeaders headers = new HttpHeaders();
	     headers.setLocation(ucBuilder.path("/student/{id}").buildAndExpand(subject.getSubject_id()).toUri());
	     return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	
    }
    
  //-------------------Update a subject--------------------------------------------------------  
    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Subject> updateSubject(@PathVariable("id") int id, @RequestBody Subject subject) {
        System.out.println("Updating Subject " + id);
          
        Subject currSubj = subjectService.getSubject(id);
          
        if (currSubj==null) {
            System.out.println("Subject with id " + id + " not found");
            return new ResponseEntity<Subject>(HttpStatus.NOT_FOUND);
        }
  
        currSubj.setName(subject.getName());
        currSubj.setTeacher(subject.getTeacher());
          
        subjectService.updateSubject(currSubj);
        return new ResponseEntity<Subject>(currSubj, HttpStatus.OK);
    }
	
    //-------------------Delete a subject--------------------------------------------------------   
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Subject> deleteSubject(@PathVariable("id") int id) {
        System.out.println("Fetching & Deleting Subject with id " + id);
  
        Subject subject = subjectService.getSubject(id);
        if (subject == null) {
            System.out.println("Unable to delete. Subject with id " + id + " not found");
            return new ResponseEntity<Subject>(HttpStatus.NOT_FOUND);
        }
  
        subjectService.deleteSubject(id);
        return new ResponseEntity<Subject>(HttpStatus.NO_CONTENT);
    }
  
}
