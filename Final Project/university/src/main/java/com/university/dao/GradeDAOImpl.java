package com.university.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.university.model.Grade;


@Repository
public class GradeDAOImpl implements GradeDAO{
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public void addGrade(Grade grade) {
		
		getCurrentSession().save(grade);		
	}

	
	
}
