package com.university.dao;

import java.util.List;

import com.university.model.Subject;

public interface SubjectDAO {
	
	public void addSubject(Subject subject);
	public void updateSubject(Subject subject);
	public Subject getSubject(int subjectId);
	public void deleteSubject(int subjectId);
	public List<Subject> getAllSubjects();
	
	
}
