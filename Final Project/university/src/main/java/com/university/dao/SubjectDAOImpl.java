package com.university.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.university.model.Subject;

@Repository
public class SubjectDAOImpl implements SubjectDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}
	
	public void addSubject(Subject subject) {
		
		getCurrentSession().save(subject);
		
	}

	public void updateSubject(Subject subject) {
		
		Subject subjectToUpdate = getSubject(subject.getSubject_id());
		subjectToUpdate.setName(subject.getName());
		getCurrentSession().update(subject);
		
	}

	public Subject getSubject(int subjectId) {
		
		Subject subject = (Subject) getCurrentSession().get(Subject.class, subjectId);
		return subject;
		
	}

	public void deleteSubject(int subjectId) {
		
		Subject subject = getSubject(subjectId);
		if(subject!=null){
			getCurrentSession().delete(subject);
		}
		
	}

	@SuppressWarnings("unchecked")
	public List<Subject> getAllSubjects() {
		
		Criteria criteria = getCurrentSession().createCriteria(Subject.class);
		return criteria.list();
		
	}


	
	
	
}
