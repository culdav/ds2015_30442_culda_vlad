package com.university.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.university.model.User;

@Repository
public class UserDAOImpl implements UserDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession(){
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		Criteria criteria = getCurrentSession().createCriteria(User.class);
		return criteria.list();
	}

	public User getUser(String username, String password) {
		
		Query q = getCurrentSession().createQuery("from User as u where u.username = :username and u.password = :password");
		q.setString("username", username);
		q.setString("password", password);
		User result = (User) q.list().get(0);
		return result;
	}

	public User getByUsername(String username) {
		
		Query q = getCurrentSession().createQuery("from User as u where u.username = :username");
		q.setString("username", username);
		User result = (User) q.list().get(0);
		return result;
		
	}

	@Override
	public void deleteUser(int id) {
		
		User user = getUserById(id);
		if(user != null){
			getCurrentSession().delete(user);
		}
	}

	@Override
	public User getUserById(int id) {
		
		return (User) getCurrentSession().get(User.class, id);
		
	}

	@Override
	public void createUser(User user) {
		// TODO Auto-generated method stub
		getCurrentSession().save(user);
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		getCurrentSession().update(user);
		
	}

}
