package com.university.model;


import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonView;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="grades")
public class Grade implements Serializable{
	
	
	public int grade_id;
	
	public int grade;
	
	@Column
    @ElementCollection(targetClass=Subject.class)
	@LazyCollection(LazyCollectionOption.FALSE)
	public Subject subject;
	
	@Column
    @ElementCollection(targetClass=User.class)
	@LazyCollection(LazyCollectionOption.FALSE)
	public Collection<User> users = new LinkedHashSet<User>(0);
	
	@Id
	@GeneratedValue(strategy= IDENTITY)
	@Column(name = "GRADE_ID", unique = true, nullable = false)
	public int getGrade_id() {
		return grade_id;
	}

	public void setGrade_id(int grade_id) {
		this.grade_id = grade_id;
	}
	
	public int getGrade() {
		return grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "grades")
	public Collection<User> getUsers() {
		return users;
	}

	public void setUsers(Collection<User> user) {
		this.users = user;
	}
	
	@OneToOne( cascade=CascadeType.ALL)  //mappedBy="grade",
	@JoinColumn(name="SUBJECT_ID")
	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	
	
	
	
//	@OneToMany(cascade=CascadeType.ALL, mappedBy = "grade")
//	public Collection<Subject> getSubjects() {
//		return subjects;
//	}
//
//	public void setSubjects(Collection<Subject> subjects) {
//		this.subjects = subjects;
//	}
	
	
	
	
	
	
	

}
