package com.university.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.university.model.Grade;



@Entity
@Table(name="user", uniqueConstraints = {
		@UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "password"),
		@UniqueConstraint(columnNames = "type"),
		@UniqueConstraint(columnNames = "name"),
		@UniqueConstraint(columnNames = "mail"),
		@UniqueConstraint(columnNames = "phone"),})
public class User implements Serializable{
	
	
	private Integer user_id;
	
	private String username;
	
	private String password;
	
	private String type;
	
	private String name;
	
	private String mail;
	
	private String phone;
	
	
	@Column
    @ElementCollection(targetClass=Grade.class)
	@LazyCollection(LazyCollectionOption.FALSE)
	public Collection<Grade> grades = new LinkedHashSet<Grade>(0);
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "user_id", unique = false, nullable = false)
	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String login) {
		this.username = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getPhone(){
		return this.phone;
	}
	public void setPhone(String phone){
		this.phone = phone;
	}
	

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "user_grades", joinColumns = { 
		@JoinColumn(name = "user_id", nullable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "grade_id", 
		nullable = false) })
	public Collection<Grade> getGrades() {
		return grades;
	}

	public void setGrades(Collection<Grade> grades) {
		this.grades = grades;
	}
	
	
	
	
	
	
	
	
	
	
	

}
