package com.university.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="subject")
public class Subject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int subject_id;
	
	private String name;
	
	@Column
    @ElementCollection(targetClass=User.class)
	@LazyCollection(LazyCollectionOption.FALSE)
	public User teacher;
	

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "SUBJECT_ID", insertable=false, updatable=false, nullable=false)
	public int getSubject_id() {
		return subject_id;
	}

	public void setSubject_id(int subject_id) {
		this.subject_id = subject_id;
	}	
	
	@Column(name = "NAME", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@OneToOne( cascade=CascadeType.ALL)  //mappedBy="grade",
	@JoinColumn(name="TEACHER_ID")
	public User getTeacher() {
		return teacher;
	}

	public void setTeacher(User user) {
		this.teacher = user;
	}
	
	
	
}