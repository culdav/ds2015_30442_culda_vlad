package webServices;

import BL.PackageBL;
import Model.Package;

import javax.xml.ws.BindingType;
import java.util.List;


@javax.jws.WebService()
@BindingType("http://java.sun.com/xml/ns/jaxws/2003/05/soap/bindings/HTTP/")
public class WebService implements PackageIf {

    public void addPackage(Package aPackage)
    {
        PackageBL packageBL = new PackageBL();
        packageBL.addPackage(aPackage);
    }

    public void deletePackage(Package aPackage)
    {
        PackageBL packageBL = new PackageBL();
        packageBL.deletePackage(aPackage);
    }

    public void registerPackage(Package aPackage) {
        PackageBL packageBL = new PackageBL();
        packageBL.registerPackage(aPackage);
    }

    public void updatePackage(Package aPackage) {
        PackageBL packageBL = new PackageBL();
        packageBL.updatePackage(aPackage);
    }

    public List<Package>getPackages(){
        PackageBL packageBL = new PackageBL();
        return packageBL.getAllPackages();
    }

    public List<Package>getUserSpecifiedPackage(int userID)
    {
        PackageBL packageBL = new PackageBL();
        return   packageBL.getUserSpecifiedPackage(userID);
    }
}
