package webServices;

import Model.Package;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.BindingType;


@WebService()
@BindingType("http://java.sun.com/xml/ns/jaxws/2003/05/soap/bindings/HTTP/")

public interface PackageIf {

    @WebMethod
    public void addPackage(Package aPackage);

    @WebMethod
    public void deletePackage(Package aPackage);

    @WebMethod
    public void registerPackage(Package apPackage);

    @WebMethod
    public void updatePackage(Package apPackage);

}
