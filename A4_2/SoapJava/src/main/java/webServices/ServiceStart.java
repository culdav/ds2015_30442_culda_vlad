package webServices;
import javax.xml.ws.Endpoint;


public class ServiceStart
{
  public static void main(String[] args) {

    Object implementor = new webServices.WebService();
    String address = "http://localhost:9000/test  ";
    Endpoint.publish(address, implementor);

  }

}
