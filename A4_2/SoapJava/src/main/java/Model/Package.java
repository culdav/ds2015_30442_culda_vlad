package Model;
import javax.jws.WebService;


@WebService()
public class Package {

    private int packagesID;

    private int packageSender;

    private int packageReceiver;

    private String packageName ;

    private String packageDescription ;

    private String packageSenderCity ;

    private String packageDestionationCity;

    private String packageTracking;


    public int getPackageSender() {
        return packageSender;
    }

    public void setPackageSender(int packageSender) {
        this.packageSender = packageSender;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Integer getPackagesID() {
        return packagesID;
    }

    public void setPackagesID(Integer packageID) {
        this.packagesID = packageID;
    }
    public String getPackageDescription() {
        return packageDescription;
    }

    public int getPackageReceiver() {
        return packageReceiver;
    }

    public void setPackageReceiver(int packageReceiver) {
        this.packageReceiver = packageReceiver;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public String getPackageSenderCity() {
        return packageSenderCity;
    }

    public void setPackageSenderCity(String packageSenderCity) {
        this.packageSenderCity = packageSenderCity;
    }

    public String getPackageDestionationCity() {
        return packageDestionationCity;
    }

    public void setPackageDestionationCity(String packageDestionationCity) {
        this.packageDestionationCity = packageDestionationCity;
    }

    public String getPackageTracking() {
        return packageTracking;
    }

    public void setPackageTracking(String packageTracking) {
        this.packageTracking = packageTracking;
    }




    @Override
    public String toString() {
        return "Package{" +
                "packageTracking=" + packageTracking +
                ", packageDestionationCity='" + packageDestionationCity + '\'' +
                ", packageSenderCity='" + packageSenderCity + '\'' +
                ", packageDescription='" + packageDescription + '\'' +
                ", packageName='" + packageName + '\'' +
                ", packageReceiver=" + packageReceiver +
                ", packageSender=" + packageSender +
                ", packageID=" + packagesID +
                '}';
    }
}
