package BL;

import DAO.PackageDataAccess;
import Model.Package;
import org.hibernate.Session;
import persistence.HibernateUtil;

import javax.jws.WebService;
import java.util.List;

public class PackageBL {

    public void addPackage(Package aPackage)
    {
        PackageDataAccess dataAccess = new PackageDataAccess();
        dataAccess.addPackage(aPackage);
    }

    public void deletePackage(Package aPackage)
    {
        PackageDataAccess  dataAccess = new PackageDataAccess();
        dataAccess.deletePackage(aPackage);
    }

    public void registerPackage(Package aPackage)
    {
        PackageDataAccess  dataAccess = new PackageDataAccess();
        dataAccess.registerPackage(aPackage);
    }
    public void updatePackage(Package aPackage)
    {
        PackageDataAccess  dataAccess = new PackageDataAccess();
        dataAccess.updatePackage(aPackage);
    }

    public List<Package> getAllPackages() {
        PackageDataAccess dataAccess = new PackageDataAccess();
        return dataAccess.getAllPackages();
    }

    public List<Package> getUserSpecifiedPackage(int userID)
    {
        PackageDataAccess dataAccess = new PackageDataAccess();
        return  dataAccess.getUserSpecifiedPackages(userID);

    }

}
