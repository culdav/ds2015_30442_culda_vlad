package DAO;
import Model.Package;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import persistence.HibernateUtil;

import java.util.List;

public class PackageDataAccess {

    public void addPackage(Package aPackage)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        System.out.println(aPackage.toString());
        session.save(aPackage);

        session.getTransaction().commit();
    }

    public void deletePackage(Package apPackag)
    {

    }

    public void registerPackage(Package apPackage)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        apPackage.setPackageDescription("");
        session.update(apPackage);
    }

    public void updatePackage(Package apPackage)
    {

    }

    public List<Package> getAllPackages() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Package> packages = null;
        try
        {
            tx = session.beginTransaction();
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            packages = session.createQuery("FROM Model.Package ").list();
        }
        catch (HibernateException e)
        {
            if (tx != null) {
                tx.rollback();
            }
        }
        finally
        {
            session.close();
        }
        return packages;
    }

    public List<Package> getUserSpecifiedPackages(int userID)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        List<Package> packages = null;
        try
        {
            tx = session.beginTransaction();
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from Model.Package where packageReceiver = :receiver ");
            query.setParameter("receiver", userID);

            packages = query.list();
        }
        catch (HibernateException e)
        {
            if (tx != null) {
                tx.rollback();
            }
        }
        finally
        {
            session.close();
        }
        return packages;
    }
}
