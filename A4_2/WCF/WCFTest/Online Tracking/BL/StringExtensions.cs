﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Online_Tracking.BL
{
    public static  class StringExtensions
    {

    public static int ToBoolean(this string value)
    {
        switch (value.ToLower())
        {
            case "true":
                return 1;
            case "t":
                return 1;
            case "1":
                return 1;
            case "0":
                return 0;
            case "false":
                return 0;
            case "f":
                return 0;
            default:
                throw new InvalidCastException("You can't cast a weird value to a bool!");
        }
     }
   }
 }