﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Online_Tracking.ClientService;
using System.Threading.Tasks;
using Online_Tracking.PackageService;

namespace Online_Tracking.BL
{
    class UserBL
    {
        public User CeckUser(string username, string password)
        {
            UserServiceClient client = new UserServiceClient();
            User user = new User() { userName = username, userPassword = password };
            user = client.GetClient(user);
            return user;
        }

        public List<package> ReturnUserPackages(int UserID)
        {
            WebServiceClient packageService = new WebServiceClient();
            List<package> packages = packageService.getUserSpecifiedPackage(UserID).ToList();
            return packages;
          
        }

        public void RegisterUser(string username, string password)
        {
            UserServiceClient client = new UserServiceClient();
            User user = new User() { userName = username, userPassword = password };
            client.AddUser(user);
            
        }

    }
}
