﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Online_Tracking.PackageService;
using Online_Tracking.PackageService;

namespace Online_Tracking.BL
{
    public class AdminBL
    {
        public void AddPackage(String packageSender, String packageReceiver, String packageName, String packageDescription,
                               String packageSenderCity, String packageDestinationCity, String packageTraking)
        {
            package package = new package();
            package.packageSender = Int32.Parse(packageSender);
            package.packageReceiver = Int32.Parse(packageReceiver);
            package.packageName = packageName;
            package.packageDescription = packageDescription;
            package.packageSenderCity = packageSenderCity;
            package.packageDestionationCity = packageDestinationCity;
            package.packageTracking = packageTraking;
            WebServiceClient packageService = new WebServiceClient();
            packageService.addPackage(package);

        }

        public void DeletePackage()
        {

        }

        public void UpdatePackage()
        {

        }

        public void RegisterPackage()
        {

        }

        public List<package> GetPackages()
        {
            WebServiceClient packageService = new WebServiceClient();
            List<package> packages = packageService.getPackages().ToList();
            return packages;
        }

    }
}
