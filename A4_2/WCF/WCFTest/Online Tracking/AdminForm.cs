﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Online_Tracking.PackageService;

namespace Online_Tracking
{
    public partial class AdminForm : Form
    {
        public AdminForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void buttonAddPackage_Click(object sender, EventArgs e)
        {
            AdminViews.FormAddPackage addPackage = new AdminViews.FormAddPackage();
            addPackage.Show();

        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            Online_Tracking.BL.AdminBL adminBL = new Online_Tracking.BL.AdminBL();
            var packages = adminBL.GetPackages();
            var bindingList = new BindingList<package>(packages);
            var source = new BindingSource(bindingList, null);
            dataGridView1.DataSource = source;

        }
    }
}
