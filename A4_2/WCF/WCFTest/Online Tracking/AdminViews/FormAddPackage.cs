﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Tracking.AdminViews
{
    public partial class FormAddPackage : Form
    {
        public FormAddPackage()
        {
            InitializeComponent();
        }

        private void buttonSavePackage_Click(object sender, EventArgs e)
        {
            BL.AdminBL adminBL = new BL.AdminBL();
            adminBL.AddPackage(textBoxPackageSender.Text, textBoxPackageReceiver.Text, textBoxPackageName.Text, textBoxPackageDescription.Text,
                               textBoxSenderCity.Text, textBoxDestinationCity.Text, textBoxPackageTraking.Text);
            MessageBox.Show("Succes!");
            this.Dispose();
                        
        }
    }
}
