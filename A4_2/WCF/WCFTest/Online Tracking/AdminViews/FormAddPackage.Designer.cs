﻿namespace Online_Tracking.AdminViews
{
    partial class FormAddPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSavePackage = new System.Windows.Forms.Button();
            this.textBoxPackageSender = new System.Windows.Forms.TextBox();
            this.textBoxPackageReceiver = new System.Windows.Forms.TextBox();
            this.textBoxPackageName = new System.Windows.Forms.TextBox();
            this.textBoxPackageDescription = new System.Windows.Forms.TextBox();
            this.textBoxSenderCity = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBoxDestinationCity = new System.Windows.Forms.TextBox();
            this.textBoxPackageTraking = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonSavePackage
            // 
            this.buttonSavePackage.Location = new System.Drawing.Point(711, 75);
            this.buttonSavePackage.Name = "buttonSavePackage";
            this.buttonSavePackage.Size = new System.Drawing.Size(152, 23);
            this.buttonSavePackage.TabIndex = 0;
            this.buttonSavePackage.Text = "Save Package";
            this.buttonSavePackage.UseVisualStyleBackColor = true;
            this.buttonSavePackage.Click += new System.EventHandler(this.buttonSavePackage_Click);
            // 
            // textBoxPackageSender
            // 
            this.textBoxPackageSender.Location = new System.Drawing.Point(21, 34);
            this.textBoxPackageSender.Name = "textBoxPackageSender";
            this.textBoxPackageSender.Size = new System.Drawing.Size(100, 20);
            this.textBoxPackageSender.TabIndex = 1;
            this.textBoxPackageSender.Text = "Sender";
            // 
            // textBoxPackageReceiver
            // 
            this.textBoxPackageReceiver.Location = new System.Drawing.Point(127, 34);
            this.textBoxPackageReceiver.Name = "textBoxPackageReceiver";
            this.textBoxPackageReceiver.Size = new System.Drawing.Size(100, 20);
            this.textBoxPackageReceiver.TabIndex = 2;
            this.textBoxPackageReceiver.Text = "Receiver";
   
            // 
            // textBoxPackageName
            // 
            this.textBoxPackageName.Location = new System.Drawing.Point(233, 34);
            this.textBoxPackageName.Name = "textBoxPackageName";
            this.textBoxPackageName.Size = new System.Drawing.Size(100, 20);
            this.textBoxPackageName.TabIndex = 3;
            this.textBoxPackageName.Text = "Name";
            // 
            // textBoxPackageDescription
            // 
            this.textBoxPackageDescription.Location = new System.Drawing.Point(339, 34);
            this.textBoxPackageDescription.Name = "textBoxPackageDescription";
            this.textBoxPackageDescription.Size = new System.Drawing.Size(100, 20);
            this.textBoxPackageDescription.TabIndex = 4;
            this.textBoxPackageDescription.Text = "Description";
 
            // 
            // textBoxSenderCity
            // 
            this.textBoxSenderCity.Location = new System.Drawing.Point(445, 34);
            this.textBoxSenderCity.Name = "textBoxSenderCity";
            this.textBoxSenderCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxSenderCity.TabIndex = 5;
            this.textBoxSenderCity.Text = "SenderCity";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(551, 34);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "SenderCity";
            // 
            // textBoxDestinationCity
            // 
            this.textBoxDestinationCity.Location = new System.Drawing.Point(657, 34);
            this.textBoxDestinationCity.Name = "textBoxDestinationCity";
            this.textBoxDestinationCity.Size = new System.Drawing.Size(100, 20);
            this.textBoxDestinationCity.TabIndex = 7;
            this.textBoxDestinationCity.Text = "DestinationCity";
            // 
            // textBoxPackageTraking
            // 
            this.textBoxPackageTraking.Location = new System.Drawing.Point(763, 34);
            this.textBoxPackageTraking.Name = "textBoxPackageTraking";
            this.textBoxPackageTraking.Size = new System.Drawing.Size(100, 20);
            this.textBoxPackageTraking.TabIndex = 8;
            this.textBoxPackageTraking.Text = "Traking";
            // 
            // FormAddPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 110);
            this.Controls.Add(this.textBoxPackageTraking);
            this.Controls.Add(this.textBoxDestinationCity);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBoxSenderCity);
            this.Controls.Add(this.textBoxPackageDescription);
            this.Controls.Add(this.textBoxPackageName);
            this.Controls.Add(this.textBoxPackageReceiver);
            this.Controls.Add(this.textBoxPackageSender);
            this.Controls.Add(this.buttonSavePackage);
            this.Name = "FormAddPackage";
            this.Text = "FormAddPackage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSavePackage;
        private System.Windows.Forms.TextBox textBoxPackageSender;
        private System.Windows.Forms.TextBox textBoxPackageReceiver;
        private System.Windows.Forms.TextBox textBoxPackageName;
        private System.Windows.Forms.TextBox textBoxPackageDescription;
        private System.Windows.Forms.TextBox textBoxSenderCity;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBoxDestinationCity;
        private System.Windows.Forms.TextBox textBoxPackageTraking;
    }
}