﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Online_Tracking.PackageService;
using Online_Tracking.ClientService;
namespace Online_Tracking
{
    public partial class ClientForm : Form
    {
      private User _user = null;
        public ClientForm(User user)
        {
            _user = user;
            InitializeComponent();
        }

        private void buttonRelaod_Click(object sender, EventArgs e)
        {
            BL.UserBL userBL = new BL.UserBL();
            int userID = _user.Id;
            var userPackages =  userBL.ReturnUserPackages(userID);
            var bindingList = new BindingList<package>(userPackages);
            var source = new BindingSource(bindingList, null);
            dataGridView1.DataSource = source;
        }

       
    }
}
