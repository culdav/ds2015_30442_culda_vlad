﻿using Online_Tracking.BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Online_Tracking.ClientService;
namespace Online_Tracking
{
    public partial class LogInForm : Form
    {
        public LogInForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            string password = tbPassword.Text;
            string username = tbUsername.Text;

            UserBL userBL = new UserBL();
            User user = new User();
            user = userBL.CeckUser(username, password);
            string userRole = user.userRole ;
            if (userRole != null)
            {
                if (userRole.Equals("admin"))
                {
                    this.Visible = false;
                    AdminForm adminForm = new AdminForm();
                    adminForm.Show();

                }
                if (userRole.Equals("user"))
                {
                    this.Visible = false;

                    ClientForm clientForm = new ClientForm(user);
                    clientForm.Show();

                }
            }
            else
            {
                MessageBox.Show("User not exist");
            }




                       
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show();
        }
    }
}
