﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Online_Tracking
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void buttonSignUp_Click(object sender, EventArgs e)
        {
            String username = textBoxUsername.Text;
            String password = textBoxPassword.Text;

            BL.UserBL userBL = new BL.UserBL();
            userBL.RegisterUser(username, password);
            MessageBox.Show("Try to login");

            this.Dispose();

        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {

        }
    }
}
