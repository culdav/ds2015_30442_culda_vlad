﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineTrack.Model
{
    public class Package
    {
        public string _sender { get; set; }

        public string _receiver { get; set; }

        public string _name { get; set; }

        public string _description { get; set; }

        public string _senderCity { get; set; }

        public string _destionationCity { get; set; }

        public bool _tracking { get; set; }


    }
}