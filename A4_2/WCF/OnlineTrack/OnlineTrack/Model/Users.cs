﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineTrack.Model
{
    public class User
    {

        public int Id { get; set; }

        public string userName { get; set; }

        public string userRole { get; set; }

        public string userPassword { get; set; }

        public List<Package> userPackages { get; set; }

    }
}