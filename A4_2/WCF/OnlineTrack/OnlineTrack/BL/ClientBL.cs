﻿using System;
using System.Collections.Generic;
using System.Linq;
using OnlineTrack.Model;
using System.Web;
using OnlineTrack.DAO;
namespace OnlineTrack.BL
{
    public class ClientBL
    {
        public User GetClient(User user)
        {
            ClientDataAccess clientDataAccess = new ClientDataAccess();
            var client = clientDataAccess.GetClient(user);
            return client;
        }
   
        public User GetPackages(User user)
        {
            ClientDataAccess clientDataAccess = new ClientDataAccess();
            var clientPackges = clientDataAccess.GetClientPackeges(user);
            user.userPackages = clientPackges;
            return user;
        }

        public void RegisterUser(User user)
        {
            ClientDataAccess clientDataAccess = new ClientDataAccess();
            clientDataAccess.RegisterUser(user);
        }

    }
}