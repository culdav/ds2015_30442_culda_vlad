﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using OnlineTrack.Model;
using System.Diagnostics;

using MySql.Data.MySqlClient;
using OnlineTrack.BL;

namespace OnlineTrack
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select UserService.svc or UserService.svc.cs at the Solution Explorer and start debugging.
    public class UserService : IUserService
    {
        
        public void AddUser(User user)
        {
            ClientBL clientBL = new ClientBL();
            clientBL.RegisterUser(user);

        }

        public void Edit(User user)
        {
            
        }

        public User GetClient(User user)
        {
            ClientBL clientBL = new ClientBL();
            return clientBL.GetClient(user);

        }

        public void GetClientPackages(User user)
        {
            throw new NotImplementedException();
        }
    }
}
