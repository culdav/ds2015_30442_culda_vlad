﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using OnlineTrack.Model;
namespace OnlineTrack
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserService" in both code and config file together.
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        User GetClient(User user);

        [OperationContract]
        void GetClientPackages(User user);

        [OperationContract]
        void AddUser(User user );

    }
}
