﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineTrack.Model;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Diagnostics;

namespace OnlineTrack.DAO
{
    public class ClientDataAccess
    {
        private MySqlConnection _conn;
        private string _connectionString = ConfigurationManager.ConnectionStrings["UserDbContext"].ConnectionString;
        public User GetClient(User user)
        {

            User returnedUser = new User();
            try
            {
                String procName = "checkUser";

                this._conn = new MySqlConnection(this._connectionString);
                this._conn.Open();
               
                /*MySqlCommand command = new MySqlCommand(procName, _conn);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@username", user.userName);
                command.Parameters.AddWithValue("@pswd", user.userPassword);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    returnedUser.Id = Int32.Parse(dr.GetString(0));
                    returnedUser.userName = dr.GetString(1);
                    returnedUser.userPassword = dr.GetString(2);
                    returnedUser.userRole = dr.GetString(3);
                }*/

                
                String query = "Select * from user where userName='" + user.userName + "' and userPassword='" + user.userPassword+"'";
                MySqlCommand command = new MySqlCommand(query, _conn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    returnedUser.Id = Int32.Parse(dr.GetString(0));
                    returnedUser.userName = dr.GetString(1);
                    returnedUser.userPassword = dr.GetString(2);
                    returnedUser.userRole = dr.GetString(3);
                }
                _conn.Close();
                


            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: '{0}'", e);  
                returnedUser = null;
                
            }
            return returnedUser;
        }

        public List<Package> GetClientPackeges(User client)
        {

            List<Package> userPackages = new List<Package>();
            Package returnedPackage = new Package();
            try
            {
                String procName = "getAllUsers";
                _conn = new MySqlConnection(_connectionString);
                _conn.Open();

                String query = "Select * from package where packagesReciever='" + client.Id + "'";
                MySqlCommand command = new MySqlCommand(query, _conn);
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    returnedPackage = new Package();
                    returnedPackage._sender = dr.GetString(5);
                    returnedPackage._receiver = dr.GetString(6);
                    returnedPackage._name = dr.GetString(7);
                    returnedPackage._description = dr.GetString(8);
                    returnedPackage._senderCity = dr.GetString(9);
                    returnedPackage._destionationCity = dr.GetString(10);
                    returnedPackage._tracking = dr.GetBoolean(11);

                    userPackages.Add(returnedPackage);

                }


                /*MySqlCommand command = new MySqlCommand(procName, _conn);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@usernames", client.userName);
                MySqlDataReader dr = command.ExecuteReader();*/

                _conn.Close();
            }
            catch
            {
                Console.WriteLine("Package read error");

            }

            return userPackages;

        }

        public void RegisterUser(User user)
        {
            String procName = "RegisterUser";
            this._conn = new MySqlConnection(this._connectionString);
            this._conn.Open();


            String query = "Insert into user(userName, userPassword, userRole) values('"+user.userName+"','"+user.userPassword+"','user'"+")";
            MySqlCommand command = new MySqlCommand(query, _conn);
            MySqlDataReader dr = command.ExecuteReader();

            /*MySqlCommand command = new MySqlCommand(procName, _conn);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@username", user.userName);
            command.Parameters.AddWithValue("@pswd", user.userPassword);
            MySqlDataReader dr = command.ExecuteReader();*/



            _conn.Close();
        }

        public void SearchClientPackages()
        {

        }



    }
}