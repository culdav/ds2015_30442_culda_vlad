package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;

public class ClientStart2 {
	
	    public static void main(String[] args) {
	        QueueServerConnection queue = new QueueServerConnection("localhost", 8888);
	        
	        String message;

	        while (true) {
	            try {

	                message = queue.readMessage();
	                PrintWriter writer = new PrintWriter(new FileWriter("consumer.txt", true));
	                writer.println(message);
	                writer.close();
	                System.out.println("test");

	            }
	            catch (IOException e)
	            {
	                e.printStackTrace();
	            }

	        }
	    }


}
